﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GoToTrack : MonoBehaviour {

    public Text Hscore;
    float highscore;

    void Start()
    {
        highscore = PlayerPrefs.GetFloat("highscore", highscore);
    }

    public void startScene()
    {
        SceneManager.LoadScene("Track");
        //There is bug happening when the scene gets loaded for the first time(steering input is not detected)
        //Loading it again for making the steering input to work
        SceneManager.LoadScene("Track");
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        Hscore.text = "Best:"+highscore.ToString("0.00") + "s";
    }

    //reseting highScore
    public void resetScore()
    {
        PlayerPrefs.SetFloat("highscore", 0);
        highscore = PlayerPrefs.GetFloat("highscore", highscore);
        Hscore.text = "Best:" + highscore.ToString("0.00") + "s";
    }
}
