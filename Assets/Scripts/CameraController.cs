﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    public GameObject target;
    private Vector3 offset;
	// Use this for initialization
	void Start () {
        offset = transform.position - target.transform.position;
	
	}
	
	// Update is called once per frame
	void LateUpdate () {
        float desiredAngle = target.transform.eulerAngles.y+30;
        Quaternion rotation = Quaternion.Euler(-50,desiredAngle, 0);
        transform.position = target.transform.position +(rotation * offset);
        transform.LookAt(target.transform);
    }
}
