﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameSettings : MonoBehaviour {

    
    private AudioSource[] audioOff;
    private float timer=0,count=3.0f;
    public Text time,WinText,Hscore;
    public static float highscore;

    void Awake()
    {
        Screen.orientation = ScreenOrientation.Landscape;
    }

	// Use this for initialization
	void Start () {
        Screen.autorotateToPortrait = false;
        highscore = PlayerPrefs.GetFloat("highscore", highscore);
        Debug.Log(highscore);
    }
	
	// Update is called once per frame
	void Update () {
        //Base case if any RoadBlock object error occurs
        if(transform.position.y<-105)
        {
           SceneManager.LoadScene("Track");
        }

        //Timer code
        if(Time.timeScale==1)
        {
            timer += Time.deltaTime;
            time.text=timer.ToString("0.00")+"s";
        }
        
        if (Input.GetKeyDown(KeyCode.Escape))
        {
                SceneManager.LoadScene("OpenScene");
        }
    }

    //For end detection
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("End"))
        {
            Debug.Log("Finished");
            GameOver();
        }
    }

    //End of Game
    private void GameOver()
    {
        Time.timeScale = 0;

        //For HighScore
        if(timer<highscore)
        {
            highscore = timer;
            Hscore.text = "New Best:" + highscore.ToString("0.00")+"s";
            PlayerPrefs.SetFloat("highscore", highscore);
        }
        else if(highscore==0)
        {
            highscore = timer;
            Hscore.text = "New Best:" + highscore.ToString("0.00") + "s";
            PlayerPrefs.SetFloat("highscore", highscore);
        }
        else
        {
            Hscore.text = "Best:" + highscore.ToString("0.00") + "s";
        }


        WinText.text = "Race Won!";
        audioOff = FindObjectsOfType(typeof(AudioSource)) as AudioSource[];
        foreach (AudioSource audioS in audioOff)
        {
            audioS.Stop();
        }
    }

    //for reloading scene at end
    public void reload()
    {
       // Time.timeScale = 1;
        SceneManager.LoadScene("Track");  
    }
}
